#!/bin/bash
# In advance of this script you need to do few steps.
# (1) Download Raspbian Lite at https://www.raspberrypi.org/downloads/raspbian/ .
# (2) Write .img to your Compute module via a IO board.
# More details are at https://www.raspberrypi.org/documentation/hardware/computemodule/cm-emmc-flashing.md
# (3) Before first boot, you must edit /boot/cmdline.txt. "init=..." must be removed to prevent fs resize.
# (4) Boot the Compute module

# log in as root
# sudo -s

# Set password "root" for user root.
passwd root
# check size of partitions
#df -h
# resize the original partition
parted /dev/mmcblk0 resizepart 2 2G
resize2fs /dev/mmcblk0p2
# create new partition for slotcar
a=$(fdisk -l /dev/mmcblk0 | grep mmcblk0p2 | awk '{print $3}')
echo -en "n\np\n3\n$(($a+1))\n\nw\n" | fdisk /dev/mmcblk0
mkfs.ext4 /dev/mmcblk0p3
#create slotcar dir
mkdir /slotcar
mount /dev/mmcblk0p3 /slotcar/
# enable ssh
systemctl enable ssh
systemctl start ssh

systemctl stop dhcpcd.service
systemctl disable dhcpcd.service
# connect to Wi-Fi
iwconfig wlan0 mode ad-hoc
iwconfig wlan0 essid "car-control"
ifconfig wlan0 inet 192.168.1.100
# SSID="car-control"
#PSWD="sbba29DZ"
#echo -e "\nnetwork={\n\tssid=\"$SSID\"\n\tpsk=\"$PSWD\"\n}" >> /etc/wpa_supplicant/wpa_supplicant.conf
ifdown wlan0
ifup wlan0
# add ssh key
mkdir /root/.ssh
sudo touch ~/.ssh/authorized_keys
chmod 700 /root/.ssh
chmod 600 /root/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgJp0yflg7mpkQWogkLDLxq7W3vSUOc35grBU/oIa/WFfx/cPFBdcKHg2mWo0aCHzj3ynQW9rtzShrDtFBq6hq5wMrOdtySZIlGzk3zm/PxR0k4ZRSD/OUe/qR31TUo05MYg76z1FNa7iLt8bb8dXzlP0CwQSh4e6CuzfW8Gzvk3m95CW8MlF0yPtrk8m4tJkCeP1HRIAs/p50UGCU8YFhfBrkTKd6wX/xYC1uMz7tMLtJOoo+3s9ylpVEz6w1iHpsPwVhy4nGKAO1cAdLab5By5KKrMf1Xb4+GfBBYOCMkmtfifq/ovtPLOGPmf6d+3BPFJBaC+W3sWlDRYDat57v root@LAPTOP-Y700" > /root/.ssh/authorized_keys
# update date, force
sntp -s 0.cz.pool.ntp.org
# update
apt-get update
# upgrade
apt-get -y upgrade
# install java
apt-get -y install oracle-java8-jdk
java -version
# install wiringPi
cd ~
apt-get -y install git-core
git clone git://git.drogon.net/wiringPi
cd ./wiringPi
# in case you install from source
#tar xfz wiringPi-*.tar.gz
#cd wiringPi-*
./build
gpio -v
gpio readall

mkdir /slotcar/git
cd /slotcar/git
git clone https://gitlab.fel.cvut.cz/SlotcarPlatooning/slotcar-fs.git
cd ./slotcar-fs
git remote update
chmod +x ./install.sh
./install.sh

cd /slotcar
mkdir /slotcar/lib # SlotcarPi.jar libs
chown pi /slotcar -R
chgrp pi /slotcar -R
chmod u+x slotcar.sh stm32boot

# install more usefull stuff
apt-get install vim tmux -y
# in case of language settings
#raspi-config
# https://hallard.me/raspberry-pi-read-only/

# random seed
ln -s /tmp/random-seed /var/lib/systemd/random-seed
sed -i '/RemainAfterExit=yes/ a ExecStartPre=/bin/echo "" >/tmp/random-seed' /lib/systemd/system/systemd-random-seed.service
touch /tmp/random-seed
rm /var/lib/systemd/random-seed
ln -s /tmp/random-seed /var/lib/systemd/random-seed

rm /etc/mtab
ln -s /proc/self/mounts /etc/mtab
apt-get remove -y --purge wolfram-engine triggerhappy fake-hwclock anacron logrotate dbus dphys-swapfile xserver-common lightdm
apt-get autoremove --purge -y
insserv -r x11-common
insserv -r bootlogs
insserv -r alsa-utils # if you don't need alsa stuff (sound output)
insserv -r console-setup
insserv -r fake-hwclock # probably already removed at this point..
# This will put log into circular memory buffer, you will able to see log using logread command
apt-get -y install busybox-syslogd; dpkg --purge rsyslog
# enable service
systemctl enable slotcar.service
systemctl start slotcar.service

# after you copy the filesystem to the car there needs to be added execution rights to these files

# loggin is default
# user: pi
# pswd: raspberry
# after log in you should use 'sudo -i' to log as sudo and get full access
# There is no reason to be afraid of using sudo.
# The application should run only via system service.
# Manual start is recommended only for development purposes.
# Manual start:
#cd /slotcar/
#java -jar Slotcar.pi
# then user needs to open another terminal for log.
# log can be seen for example via command 'log', which shows only tail of the log.
# The log files are stored in /var/log/ directory.
