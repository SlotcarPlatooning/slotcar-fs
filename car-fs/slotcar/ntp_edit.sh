#!/bin/bash
# ------------------------------------------------------------------
# [Šimon Wernisch] ntp_edit
#          Change ntp config file and reset the service.
# ------------------------------------------------------------------

VERSION=0.1.0
SUBJECT='05a374c7-f07b-45c4-9ee7-5835ab39a169'
USAGE="Usage: ntp_edit [-hv] server_ip"

# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi


while getopts ":hv" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo $USAGE
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 1;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 1;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 1;
        ;;
    esac
  done

shift $(($OPTIND - 1))

IP1=$1

# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE


# --- Helper functions --------------------------------------------
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
  }


# --- Body --------------------------------------------------------
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
 fi

if valid_ip $IP1;
then
  sed -ri "s/^server\s*[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/server $IP1/1" /etc/ntp.conf
  systemctl restart ntp.service
  exit 0;
else
  echo "Invalid server ip"
  exit 1;
fi
# -----------------------------------------------------------------
