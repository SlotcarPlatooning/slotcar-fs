; Network interface to use - eth0, igb0 etc. (required).
ptpengine:interface = wlan0

; PTP engine preset:
; none	     = Defaults, no clock class restrictions
; slaveonly   = Slave only (clock class 255 only)
; masteronly  = Master, passive when not best master (clock class 0..127)
; masterslave = Full IEEE 1588 implementation:
; Master, slave when not best master
; (clock class 128..254)
;
; Options: none slaveonly masteronly masterslave
ptpengine:preset = masterslave

; IP transmission mode (requires IP transport) - hybrid mode uses
; multicast for sync and announce, and unicast for delay request and
; response; unicast mode uses unicast for all transmission.
; When unicast mode is selected, destination IP must be configured
; (ptpengine:unicast_address).
; Options: multicast unicast hybrid
ptpengine:ip_mode = multicast

; Transport type for PTP packets.
; Options: ipv4 ethernet
ptpengine:transport = ipv4

; Use libpcap for sending and receiving traffic (automatically enabled
; in Ethernet mode).
ptpengine:use_libpcap = Y

; Delay detection mode used - use DELAY_DISABLED for syntonisation only
; (no synchronisation).
; Options: E2E P2P DELAY_DISABLED
ptpengine:delay_mechanism = E2E

; PTP domain number.
ptpengine:domain = 0

; Time source announced in master state.
; Options: ATOMIC_CLOCK GPS TERRESTRIAL_RADIO PTP NTP HAND_SET OTHER INTERNAL_OSCILLATOR
ptpengine:ptp_timesource = NTP

; Priority 1 announced in master state,used for Best Master
; Clock selection.
ptpengine:priority1 = 128

; Priority 2 announced in master state, used for Best Master
; Clock selection.
ptpengine:priority2 = 128

; Enable panic mode: when offset from master is above 1 second, stop updating
; the clock for a period of time and then step the clock if offset remains
; above 1 second.
ptpengine:panic_mode = N

; Do not adjust the clock.
clock:no_adjust = N

; Do not reset the clock - only slew.
clock:no_reset = N

; Specify drift file
clock:drift_file = /etc/ptpd2_kernelclock.drift

; Maximum absolute frequency shift which can be applied to the clock servo
; when slewing the clock. Expressed in parts per million (1 ppm = shift of
; 1 us per second. Values above 512 will use the tick duration correction
; to allow even faster slewing. Default maximum is 512 without using tick.
clock:max_offset_ppm = 500

; One-way delay filter stiffness.
servo:delayfilter_stiffness = 6

; Clock servo PI controller proportional component gain (kP).
servo:kp = 0.100000

; Clock servo PI controller integral component gain (kI).
servo:ki = 0.001000

; How servo update interval (delta t) is calculated:
; none:     servo not corrected for update interval (dt always 1),
; constant: constant value (target servo update rate - sync interval for PTP,
; measured: servo measures how often it's updated and uses this interval.
; Options: none constant measured
servo:dt_method = constant

; Send log messages to syslog. Disabling this
; sends all messages to stdout (or speficied log file).
global:use_syslog = N

; Lock file directory: used with automatic mode-specific lock files,
; also used when no lock file is specified. When lock file
; is specified, it's expected to be an absolute path.
global:lock_directory = /var/run

; File used to log ptpd2 status information.
global:status_file = /var/run/ptpd2.status

; Enable / disable writing status information to file.
global:log_status = Y

; Status file update interval in seconds.
global:status_update_interval = 1

; Specify log file path (event log). Setting this enables logging to file.
global:log_file = /var/log/ptpd2.log

; Maximum log file size (in kB) - log file will be truncated if size exceeds
; the limit. 0 - no limit.
global:log_file_max_size = 0

; Truncate the log file every time it is (re) opened: startup and SIGHUP.
global:log_file_truncate = Y

; Specify log level (only messages at this priority or higer will be logged).
; The minimal level is LOG_ERR. LOG_ALL enables debug output if compiled with
; RUNTIME_DEBUG.
; Options: LOG_ERR LOG_WARNING LOG_NOTICE LOG_INFO LOG_ALL
global:log_level = LOG_ALL

; Specify statistics log file path. Setting this enables logging of
; statistics, but can be overriden with global:log_statistics.
global:statistics_file =

; Log timing statistics every n seconds for Sync and Delay messages
; (0 - log all).
global:statistics_log_interval = 0

; Maximum statistics log file size (in kB) - log file will be truncated
; if size exceeds the limit. 0 - no limit.
global:statistics_file_max_size = 0

; Truncate the statistics file every time it is (re) opened: startup and SIGHUP.
global:statistics_file_truncate = N

; Run in foreground - ignored when global:verbose_foreground is set
global:foreground = N

; ========= newline required in the end ==========
