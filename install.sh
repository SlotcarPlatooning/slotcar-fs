#!/bin/bash
set -e

address=$(/slotcar/addr)

# set fs read-write
# install
git fetch --all
git reset --hard origin/master
cp ./car-fs/* / -rf
systemctl daemon-reload
bash /slotcar/stop
bash /slotcar/start
# set fs read-only again

/slotcar/addr $address
set +e

systemctl stop dhcpd
systemctl disable dhcpd
