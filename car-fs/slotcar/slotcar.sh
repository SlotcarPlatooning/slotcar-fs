#!/bin/bash

readonly RESET_PIN=00 # STM reset pin
readonly BOOT_PIN=37 # STM boot pin

RW=1

gpio -g mode $RESET_PIN out # Using BCM pin numbers.
gpio -g mode $BOOT_PIN out # Using BCM pin numbers.

function disp_help {
	echo "Usage: $0 {COMMAND} arg1 arg2 ..."
    echo
	echo "COMMANDS:"
    echo "	mro     Makes / and /boot read-only."
    echo "	mrw     Makes / and /boot read-write."
	echo "	start   Starts the slotcar application."
	echo "	stop    Stops the slotcar application."
	echo "	status  Shows status of the slotcar application."
	echo "	log     Shows the log of the slotcar application."
	echo "	addr    Returns (and sets if {arg1} is set) the address of the slotcar."
	echo "	        {arg1} must be a number."
	echo "	upgrade Upgrades slotcar's file-system from Git."
	echo "	stm     Subscript for maintaining the STM."
	echo "	        For more information write 'stm help'."
	echo "	ntp     Sets ntp server."
	echo "	        {arg1} is the IP address of the server."
	echo "	help    Shows this message."
}

function disp_stm_help {
	echo " Usage: stm {COMMAND}"
	echo
	echo "COMMANDS:"
	echo "	program, p    Program STM flash."
	echo "	              Source file is \"/slotcar/slotcar_stm.bin\"."
	echo "	start         Starts STM program."
	echo "	stop          Stops STM program."
	echo "	restart       Restarts STM program."
	echo "	help, h       Display this message."
}

# --- Helper functions --------------------------------------------
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

# make read only
function mro {
	mount -o remount,ro /
	mount -o remount,ro /boot
}

# maka read write
function mrw {
	mount -o remount,rw /
	mount -o remount,rw /boot
}

# is read write
function isrw {
	if [ -w / ] && [ -w /boot ] ; then
		#echo "rw"
		return 0
	else
		#echo "ro"
		return 1
	fi
}

function stop {
	# first stop service
	# second we stop all manually started apps
	# there should be only our java app runnig hence we can stop all java apps

	systemctl stop slotcar
	# close all runnig java application
	for task in $(ps aux | grep -v grep | grep java | awk '{print $2}')
	do
	  echo "Terminating ${task}"
	  kill -SIGTERM ${task}
	done
}

function start {
# starting application as a service
# or in a console mode

CONSOLE=0
while getopts ":c" opt; do
  case $opt in
    c)
    CONSOLE=1
    ;;
  esac
done

# fist we stop all previously started applications
stop

# start application
echo "Starting slotcar application"
if [ $CONSOLE = 1 ] ; then
  echo "...in console "
  java -jar /slotcar/SlotcarPi.jar console
else
  echo "...standalone (service)"
  systemctl start slotcar
fi
}

function status {
	systemctl status slotcar
}

function addr {
conf="/slotcar/slotcar.conf"


if echo "$1" | grep -qE ^\[0-9]+$  && [ $1 -lt "255" ] ; then
	if [ $(grep LogAddr $conf -c) -gt "0" ] ; then
		sed -i 's/LogAddr=[0-9]*/LogAddr='"$1"'/' $conf

		echo -e "127.0.1.1\tslotcar$1" >> /etc/hosts
		#sudo /bin/hostname "slotcar$1"
		echo "slotcar$1" > /etc/hostname
		/etc/init.d/hostname.sh
		i=$1
		((i = i + 100))
		#echo $i
		sed -r -i "s/address 192\.168\.1\.[0-9]{1,3}/address 192.168.1.$i/" /etc/network/interfaces
		#sed -r -i 's/address ([0-9]{1,3}\.){3}[0-9]{1,3}/address 192.168.1.74/' /etc/network/interfaces

	else
		echo  "LogAddr=$1" >> $conf
	fi
fi
grep "LogAddr" $conf | grep "[0-9]*" -o
}

function upgrade {
set -e
ping -i 2 -c 1 -w 60 gitlab.fel.cvut.cz

gpio mode 23 out
gpio write 23 1

cd /slotcar/git/slotcar-fs
git remote update
UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @{0})
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @{0} "$UPSTREAM")
# if [ $LOCAL = $REMOTE ]; then
#     echo "Up-to-date"
# elif [ $LOCAL = $BASE ]; then
#     echo "Need to pull"
# elif [ $REMOTE = $BASE ]; then
#     echo "Need to push"
# else
#     echo "Diverged"
# fi

if [ $LOCAL = $REMOTE ]; then
  echo "Up-to-date."
else
# it is not synchronized with remote
# upgrading, also destroying all local changes
  bash ./install.sh
fi
}

### STM ###
function stm_boot {
	gpio -g write $BOOT_PIN $1
}

function stm_start {
	stm_boot 0
	gpio -g write $RESET_PIN 1
}

function stm_stop {
	gpio -g write $RESET_PIN 0
}

function stm {
	case "$1" in
		"program"|"p")
			stm_boot 0
			/slotcar/stm32boot -p /slotcar/slotcar_stm.bin 0x08000000
			stm_boot 1
			;;
		"start")
			stm_start
			;;
		"stop")
			stm_stop
			;;
		"restart"|"r")
			stm_stop
			sleep 0.01
			stm_start
			;;
		"help"|"h")
			disp_stm_help
			;;
		*)
			echo "Wrong STM command \"$1\""
			echo
			disp_stm_help
			;;
	esac
}

### NTP ###
function set_ntp_server {
	if valid_ip $1;
	then
	  sed -ri "0,/^server\s*[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ s/^server\s*[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/server $1/1" /etc/ntp.conf
    varIP=`ip addr show | grep -Po 'inet \K[\d.]+'`
    if test "${varIP#*$1}" != "$varIP"
  then
      sed -ri "s/stratum\s*[0-9]+/stratum 5/1" /etc/ntp.conf
    else
      sed -ri "s/stratum\s*[0-9]+/stratum 10/1" /etc/ntp.conf
    fi
	  systemctl restart ntp.service
	else
	  echo "Invalid server ip"
	  exit 1;
	fi
}

function conditional_mrw {
	if isrw; then
		return 1
	else
		mrw
		return 0
	fi
}

# root check
if [ "$(id -u)" != "0" ]; then
    echo "Sorry, you are not root."
    exit 1
fi

if (( $# == 0 )); then
	echo "Sorry, you passed no command."
	echo
	disp_help
	exit 1
fi



case "$1" in
"mro")
	mro
	;;
"mrw")
	mrw
	;;
"start"|"restart")
	start $2
	;;
"stop")
	stop
	;;
"status")
	status
	;;
"log")
	/bin/tailf /var/log/slotcar.log*
	;;
"addr")
	conditional_mrw
	RW=$?
	addr $2
	;;
"upgrade")
	conditional_mrw
	RW=$?
	upgrade
	;;
"stm")
	stm $2
	;;
"ntp")
	conditional_mrw
	RW=$?
	set_ntp_server $2
	;;
"help")
	disp_help
	;;
*)
	echo "Wrong command \"$1\""
	echo
	disp_help
	;;
esac


if (( $RW == 0 )); then
	mro
fi
